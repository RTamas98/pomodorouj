const startBtn = document.querySelector('.main__button--start');
const pauseBtn = document.querySelector('.main__button--pause');
const restartBtn = document.querySelector('.main__button--reset');

let pomodoroMainMin = document.querySelector('.main__pomodoro__time--min');
let pomodoroMainSec = document.querySelector('.main__pomodoro__time--sec');
let pomodoromin = document.querySelector('.main__pomodoro--min');
let shortmin = document.querySelector('.main__short--min');
let longmin = document.querySelector('.main__long--min');
let pomodorosec = document.querySelector('.main__pomodoro--sec');
let shortsec = document.querySelector('.main__short--sec');
let longsec = document.querySelector('.main__long--sec');
let wasPause = true;
let pomodoroName = document.querySelector('.main__pomodoro--name')

const pomodoroMinusBtn = document.querySelector('.main__pomodoro--minus');
const pomodoroPlusBtn = document.querySelector('.main__pomodoro--plus');
const shortMinusBtn = document.querySelector('.main__short--minus');
const shortPlusBtn = document.querySelector('.main__short--plus');
const longMinusBtn = document.querySelector('.main__long--minus');
const longPlusBtn = document.querySelector('.main__long--plus');

let sound = new Audio('music/beep.mp3')
let count = 1;
let startTimer = null;
let istrue = true;
let time;

pomodoroMinusBtn.addEventListener('click', () => {
    if (pomodoromin.innerHTML > 0) {
        pomodoromin.innerHTML--;

    }
});

pomodoroPlusBtn.addEventListener('click', () => {
    pomodoromin.innerHTML++;
});

shortMinusBtn.addEventListener('click', () => {
    if (shortmin.innerHTML > 0) {
        shortmin.innerHTML--;

    }
});

shortPlusBtn.addEventListener('click', () => {
    shortmin.innerHTML++;
});

longMinusBtn.addEventListener('click', () => {
    if (longmin.innerHTML > 0) {
        longmin.innerHTML--;

    }
});

longPlusBtn.addEventListener('click', () => {
    longmin.innerHTML++;
});

startBtn.addEventListener('click', () => {
    if (!wasPause) {
        pause();
    } else {
        start();
        wasPause = false;
    }
});

pauseBtn.addEventListener('click', () => {
    clearInterval(startTimer);
    startTimer = null;
    pauseBtn.style.display = 'none';
    startBtn.style.display = 'inline-block';
    wasPause = true;
})

restartBtn.addEventListener('click', () => {

    pomodoroMainMin.innerHTML = '25';
    pomodoroMainSec.innerHTML = '00';
    pomodoromin.innerHTML = '25';
    pomodorosec.innerHTML = '00';
    shortmin.innerHTML = '5';
    shortsec.innerHTML = '00';
    longmin.innerHTML = '25';
    shortsec.innerHTML = '00';

    clearInterval(startTimer);
    startTimer = null;
    count = 1;
    pomodoroName.innerHTML = 'Pomodoro 1';
    pauseBtn.style.display = 'none';
    startBtn.style.display = 'inline-block';
    wasPause = true;

    time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(pomodoromin.innerText)*60+parseInt(pomodorosec.innerText));
    canvasBoard(time);

    pomodoroMinusBtn.disabled = false;
    pomodoroPlusBtn.disabled = false;
    shortMinusBtn.disabled = false;
    shortPlusBtn.disabled = false;
    longMinusBtn.disabled = false;
    longPlusBtn.disabled = false;

})

function start() {
    if (startTimer == null) {
        Timer();
        startTimer = setInterval(Timer,1000);
    }
    startBtn.style.display = 'none';
    pauseBtn.style.display = 'inline-block';
}

function pause() {
    startTimer = setInterval(Timer,1000);
    clearInterval(startTimer)
    pauseBtn.style.display = 'none';
    startBtn.style.display = 'inline-block';

}

function Timer() {
    switch (count) {
        case 1:
            if(istrue){
                pomodoroMainMin.innerHTML = pomodoromin.innerHTML;
                pomodoroMainSec.innerHTML = pomodorosec.innerHTML;
                istrue = false;
            }
            pomodoroMinusBtn.disabled = true;
            pomodoroPlusBtn.disabled = true;
            shortMinusBtn.disabled = false;
            shortPlusBtn.disabled = false;
            longMinusBtn.disabled = false;
            longPlusBtn.disabled = false;
            time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(pomodoromin.innerText)*60+parseInt(pomodorosec.innerText));
            pomodoroTimer();
            break;
        case 2:
            if(!istrue){
                pomodoroMainMin.innerHTML = shortmin.innerHTML;
                pomodoroMainSec.innerHTML = shortsec.innerHTML;
                istrue = true;
                sound.play();
            }
            pomodoroName.innerHTML = 'Short Break';
            pomodoroMinusBtn.disabled = false;
            pomodoroPlusBtn.disabled = false;
            shortMinusBtn.disabled = true;
            shortPlusBtn.disabled = true;
            longMinusBtn.disabled = false;
            longPlusBtn.disabled = false;
            time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(shortmin.innerText)*60+parseInt(shortsec.innerText));
            timerShortBreak();
            break;
        case 3:
            if(istrue){
                pomodoroMainMin.innerHTML = pomodoromin.innerHTML;
                pomodoroMainSec.innerHTML = pomodorosec.innerHTML;
                istrue = false;
                sound.play();
            }
            pomodoroName.innerHTML = 'Pomodoro 2';
            pomodoroMinusBtn.disabled = true;
            pomodoroPlusBtn.disabled = true;
            shortMinusBtn.disabled = false;
            shortPlusBtn.disabled = false;
            longMinusBtn.disabled = false;
            longPlusBtn.disabled = false;
            time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(pomodoromin.innerText)*60+parseInt(pomodorosec.innerText));
            pomodoroTimer()
            break;
        case 4:
            if(!istrue){
                pomodoroMainMin.innerHTML = shortmin.innerHTML;
                pomodoroMainSec.innerHTML = shortsec.innerHTML;
                istrue = true;
                sound.play();
            }
            pomodoroName.innerHTML = 'Short Break';
            pomodoroMinusBtn.disabled = false;
            pomodoroPlusBtn.disabled = false;
            shortMinusBtn.disabled = true;
            shortPlusBtn.disabled = true;
            longMinusBtn.disabled = false;
            longPlusBtn.disabled = false;
            time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(shortmin.innerText)*60+parseInt(shortsec.innerText));
            timerShortBreak();
            break;
        case 5:
            if(istrue){
                pomodoroMainMin.innerHTML = pomodoromin.innerHTML;
                pomodoroMainSec.innerHTML = pomodorosec.innerHTML;
                istrue = false;
                sound.play();
            }
            pomodoroName.innerHTML = 'Pomodoro 3';
            pomodoroMinusBtn.disabled = true;
            pomodoroPlusBtn.disabled = true;
            shortMinusBtn.disabled = false;
            shortPlusBtn.disabled = false;
            longMinusBtn.disabled = false;
            longPlusBtn.disabled = false;
            time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(pomodoromin.innerText)*60+parseInt(pomodorosec.innerText));
            pomodoroTimer();
            break;
        case 6:
            if(!istrue){
                pomodoroMainMin.innerHTML = shortmin.innerHTML;
                pomodoroMainSec.innerHTML = shortsec.innerHTML;
                istrue = true;
                sound.play();
            }
            pomodoroName.innerHTML = 'Short Break';
            pomodoroMinusBtn.disabled = false;
            pomodoroPlusBtn.disabled = false;
            shortMinusBtn.disabled = true;
            shortPlusBtn.disabled = true;
            longMinusBtn.disabled = false;
            longPlusBtn.disabled = false;
            time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(shortmin.innerText)*60+parseInt(shortsec.innerText));
            timerShortBreak();
            break;
        case 7:
            if(istrue){
                pomodoroMainMin.innerHTML = pomodoromin.innerHTML;
                pomodoroMainSec.innerHTML = pomodorosec.innerHTML;
                istrue = false;
                sound.play();
            }
            pomodoroName.innerHTML = 'Pomodoro 4';
            pomodoroMinusBtn.disabled = true;
            pomodoroPlusBtn.disabled = true;
            shortMinusBtn.disabled = false;
            shortPlusBtn.disabled = false;
            longMinusBtn.disabled = false;
            longPlusBtn.disabled = false;
            time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(pomodoromin.innerText)*60+parseInt(pomodorosec.innerText));
            pomodoroTimer();
            break;
        case 8:
            if(!istrue) {
                pomodoroMainMin.innerHTML = longmin.innerHTML;
                pomodoroMainSec.innerHTML = longsec.innerHTML;
                istrue = true;
                sound.play();
            }
            pomodoroName.innerHTML = 'Long Break';
            pomodoroMinusBtn.disabled = false;
            pomodoroPlusBtn.disabled = false;
            shortMinusBtn.disabled = false;
            shortPlusBtn.disabled = false;
            longMinusBtn.disabled = true;
            longPlusBtn.disabled = true;
            time = (parseInt(pomodoroMainMin.innerText)*60 + parseInt(pomodoroMainSec.innerText)) / (parseInt(longmin.innerText)*60+parseInt(longsec.innerText));
            timerLongBreak();
            break;
    }
    canvasBoard(time);

}

function pomodoroTimer() {
    if (parseInt(pomodoroMainSec.innerText) != 0 ) {
        pomodoroMainSec.innerText = parseInt(pomodoroMainSec.innerText) - 1;
    } else if (parseInt(pomodoroMainMin.innerText) != 0 && parseInt(pomodoroMainSec.innerText) == 0) {
        pomodoroMainSec.innerText = 59;
        pomodoroMainMin.innerText = parseInt(pomodoroMainMin.innerText) - 1;
    }
    else {
        count++;
        pomodoromin.innerText = '25';
        longmin.innerText = '25'
    }
}

function timerShortBreak() {
    if (parseInt(pomodoroMainSec.innerText) != 0) {
        pomodoroMainSec.innerText = parseInt(pomodoroMainSec.innerText) - 1;
    } else if (parseInt(pomodoroMainMin.innerText) != 0 && parseInt(pomodoroMainSec.innerText) == 0) {
        pomodoroMainSec.innerText = 59;
        pomodoroMainMin.innerText = parseInt(pomodoroMainMin.innerText) - 1;
    } else {
        count++;
        shortmin.innerText = '5';
        longmin.innerText = '25';
    }
}

function timerLongBreak() {
    if (parseInt(pomodoroMainSec.innerText) != 0) {
        pomodoroMainSec.innerText = parseInt(pomodoroMainSec.innerText) - 1;
    } else if (parseInt(pomodoroMainMin.innerText) != 0 && parseInt(pomodoroMainSec.innerText) == 0) {
        pomodoroMainSec.innerText = 59;
        pomodoroMainMin.innerText = parseInt(pomodoroMainMin.innerText) - 1;
    } else {
        clearInterval(startTimer);
        startTimer = null;
        count = 1;
        pauseBtn.style.display = 'none';
        startBtn.style.display = 'inline-block';
        sound.play()
        pomodoromin.innerText = '25';
        shortmin.innerText = '5';
    }
}

let board = document.getElementById('board');
board.width = 220;
board.height = 220;

let context = board.getContext('2d');

function canvasBoard(param) {
    context.clearRect(0, 0, 220, 220);
    context.strokeStyle = "#f00";
    context.lineWidth = 6;
    context.beginPath();
    context.arc(110, 110, 104, -(Math.PI / 2), Math.PI * 2 * param - (Math.PI / 2), false);
    context.stroke();
}

